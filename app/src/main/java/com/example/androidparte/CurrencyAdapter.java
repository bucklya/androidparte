package com.example.androidparte;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder> {

    private Context context;
    private FromCurrToExchListModel model;
    OnItemClickListener listener;

    public CurrencyAdapter(Context context, FromCurrToExchListModel model, OnItemClickListener listener) {
        this.context = context;
        this.model = model;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_currency, parent, false);
        return new CurrencyViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder holder, int position) {
        holder.bind(model.getExchangeModelList().get(position));
    }

    @Override
    public int getItemCount() {
        return model.getExchangeModelList().size();
    }

    static class CurrencyViewHolder extends RecyclerView.ViewHolder {

        OnItemClickListener listener;
        View root;
        TextView name;
        TextView nbu;

        public CurrencyViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            root = itemView.findViewById(R.id.currency_item);
            name = itemView.findViewById(R.id.name);
            nbu = itemView.findViewById(R.id.nbu);
        }

        public void bind(final ExchangeModel exchangeModel) {
            nbu.setText(exchangeModel.getPurchaseRateNB().toString());
            name.setText(exchangeModel.getCurrency());
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnItemClick(exchangeModel);
                }
            });
        }
    }
}