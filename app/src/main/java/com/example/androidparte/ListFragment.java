package com.example.androidparte;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFragment extends Fragment {

    private static final String DATE = "date";
    private static final String MODEL = "model";

    private String date;
    private FromCurrToExchListModel model;
    private OnItemClickListener listener;

    public ListFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (OnItemClickListener) context;
    }

    public static ListFragment newInstance(String param1, FromCurrToExchListModel param2) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putString(DATE, param1);
        args.putParcelable(MODEL, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            date = getArguments().getString(DATE);
            model = getArguments().getParcelable(MODEL);
        }
    }

    private RecyclerView recyclerView;
    private CurrencyAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = root.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        adapter = new CurrencyAdapter(root.getContext(), model, listener);
        recyclerView.setAdapter(adapter);
        return root;
    }
}
