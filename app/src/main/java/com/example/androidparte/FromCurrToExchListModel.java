package com.example.androidparte;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class FromCurrToExchListModel implements Parcelable {
    List<CurrencyModel.ExchangeRate> currencyModelList = new ArrayList<>();
    List<ExchangeModel> exchangeModelList = new ArrayList<>();

    public FromCurrToExchListModel(List<CurrencyModel.ExchangeRate> currencyModelList) {
        this.currencyModelList = currencyModelList;
        for (CurrencyModel.ExchangeRate exchangeRate : currencyModelList) {
            exchangeModelList.add(new ExchangeModel(exchangeRate.getBaseCurrency(),
                    exchangeRate.getCurrency(),
                    exchangeRate.getSaleRateNB(),
                    exchangeRate.getPurchaseRateNB(),
                    exchangeRate.getSaleRate(),
                    exchangeRate.getPurchaseRate()));
        }
    }

    protected FromCurrToExchListModel(Parcel in) {
        exchangeModelList = in.createTypedArrayList(ExchangeModel.CREATOR);
    }

    public static final Creator<FromCurrToExchListModel> CREATOR = new Creator<FromCurrToExchListModel>() {
        @Override
        public FromCurrToExchListModel createFromParcel(Parcel in) {
            return new FromCurrToExchListModel(in);
        }

        @Override
        public FromCurrToExchListModel[] newArray(int size) {
            return new FromCurrToExchListModel[size];
        }
    };

    public List<ExchangeModel> getExchangeModelList() {
        return exchangeModelList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(exchangeModelList);
    }
}
