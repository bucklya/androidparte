package com.example.androidparte;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ButtonFragment extends Fragment {

    Button cont;
    Button getData;
    EditText date;
    OnButtonClickListener onButtonClickListener;
    FromCurrToExchListModel model;
    String inputDate;

    public ButtonFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        onButtonClickListener = (OnButtonClickListener) context;
    }

    public static ButtonFragment newInstance() {
        ButtonFragment fragment = new ButtonFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_button, container, false);
        cont = root.findViewById(R.id.cont);
        getData = root.findViewById(R.id.get_data);
        date = root.findViewById(R.id.input_date);
        final FromCurrToExchListModel[] fromCurrToExchListModel = new FromCurrToExchListModel[1];
        getData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (date.getText().toString().isEmpty()) {
                    Toast.makeText(root.getContext(), "Введите дату!", Toast.LENGTH_SHORT).show();
                } else {
                    inputDate = date.getText().toString();
                    ApiService.getData(inputDate).enqueue(new Callback<CurrencyModel>() {
                        @Override
                        public void onResponse(Call<CurrencyModel> call, Response<CurrencyModel> response) {
                            if (response.isSuccessful() && response.body() != null) {
                                fromCurrToExchListModel[0] = new FromCurrToExchListModel(response.body().getExchangeRate());
                                model = fromCurrToExchListModel[0];
                            } else {
                                Toast.makeText(root.getContext(), "Что то пошло не так", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CurrencyModel> call, Throwable t) {
                            Toast.makeText(root.getContext(), "Что то пошло не так", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClickListener.OnButtonClick(inputDate, model);
            }
        });
        return root;
    }
}
