package com.example.androidparte;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class InfoFragment extends Fragment {

    private static final String ARG = "currency";

    private ExchangeModel item;

    public InfoFragment() {
    }

    public static InfoFragment newInstance(ExchangeModel model) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG, model);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            item = (ExchangeModel) getArguments().getParcelable(ARG);
        }
    }

    TextView infoName;
    TextView infoSaleNB;
    TextView infoPurchaseNB;
    TextView infoSale;
    TextView infoPurchase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_info, container, false);
        infoName = root.findViewById(R.id.info_name);
        infoSaleNB = root.findViewById(R.id.sale_nb);
        infoPurchaseNB = root.findViewById(R.id.purchase_nb);
        infoSale = root.findViewById(R.id.sale);
        infoPurchase = root.findViewById(R.id.purchase);

        infoName.setText(item.getCurrency());
        infoSaleNB.setText(item.getSaleRateNB().toString());
        infoPurchaseNB.setText(item.getPurchaseRateNB().toString());
        if (item.getSaleRate() == null) {
            infoSale.setText("");
        } else {
            infoSale.setText(item.getSaleRate().toString());
        }
        if (item.getPurchaseRate() == null) {
            infoPurchase.setText("");
        } else {
            infoPurchase.setText(item.getPurchaseRate().toString());
        }
        return root;
    }
}
