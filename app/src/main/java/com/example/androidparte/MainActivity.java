package com.example.androidparte;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements OnButtonClickListener, OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ButtonFragment.newInstance())
                .commit();
    }


    @Override
    public void OnButtonClick(String date, FromCurrToExchListModel model) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ListFragment.newInstance(date, model))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void OnItemClick(ExchangeModel model) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, InfoFragment.newInstance(model))
                .addToBackStack(null)
                .commit();
    }
}
