package com.example.androidparte;

import android.os.Parcel;
import android.os.Parcelable;

public class ExchangeModel implements Parcelable {
    private String baseCurrency;
    private String currency;
    private Double saleRateNB;
    private Double purchaseRateNB;
    private Double saleRate;
    private Double purchaseRate;

    public ExchangeModel(String baseCurrency, String currency, Double saleRateNB, Double purchaseRateNB, Double saleRate, Double purchaseRate) {
        this.baseCurrency = baseCurrency;
        this.currency = currency;
        this.saleRateNB = saleRateNB;
        this.purchaseRateNB = purchaseRateNB;
        this.saleRate = saleRate;
        this.purchaseRate = purchaseRate;
    }

    protected ExchangeModel(Parcel in) {
        baseCurrency = in.readString();
        currency = in.readString();
        if (in.readByte() == 0) {
            saleRateNB = null;
        } else {
            saleRateNB = in.readDouble();
        }
        if (in.readByte() == 0) {
            purchaseRateNB = null;
        } else {
            purchaseRateNB = in.readDouble();
        }
        if (in.readByte() == 0) {
            saleRate = null;
        } else {
            saleRate = in.readDouble();
        }
        if (in.readByte() == 0) {
            purchaseRate = null;
        } else {
            purchaseRate = in.readDouble();
        }
    }

    public static final Creator<ExchangeModel> CREATOR = new Creator<ExchangeModel>() {
        @Override
        public ExchangeModel createFromParcel(Parcel in) {
            return new ExchangeModel(in);
        }

        @Override
        public ExchangeModel[] newArray(int size) {
            return new ExchangeModel[size];
        }
    };

    public String getCurrency() {
        return currency;
    }

    public Double getSaleRateNB() {
        return saleRateNB;
    }

    public Double getPurchaseRateNB() {
        return purchaseRateNB;
    }

    public Double getSaleRate() {
        return saleRate;
    }

    public Double getPurchaseRate() {
        return purchaseRate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(baseCurrency);
        dest.writeString(currency);
        if (saleRateNB == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(saleRateNB);
        }
        if (purchaseRateNB == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(purchaseRateNB);
        }
        if (saleRate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(saleRate);
        }
        if (purchaseRate == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(purchaseRate);
        }
    }
}