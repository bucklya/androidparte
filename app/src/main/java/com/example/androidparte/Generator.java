package com.example.androidparte;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class Generator {

    private Generator() {
    }

    public static FromCurrToExchListModel generate(String date, final Context context) {
        final FromCurrToExchListModel[] fromCurrToExchListModel = new FromCurrToExchListModel[1];
        ApiService.getData(date).enqueue(new Callback<CurrencyModel>() {
            @Override
            public void onResponse(Call<CurrencyModel> call, Response<CurrencyModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    fromCurrToExchListModel[0] = new FromCurrToExchListModel(response.body().getExchangeRate());
                } else {
                    Toast.makeText(context, "Что то пошло не так", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CurrencyModel> call, Throwable t) {
                Toast.makeText(context, "Что то пошло не так", Toast.LENGTH_SHORT).show();
            }
        });
        return fromCurrToExchListModel[0];
    }
}
